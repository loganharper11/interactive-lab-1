using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Adding engine function where the scene can be managed
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour
{
    //Creating respawn variable
    public int Respawn;

    //Checking for collisions
    void OnTriggerEnter2D(Collider2D other)
    {
        //Check if collision is with objects tagged Player
        if (other.CompareTag("Player"))
        {
            //Reload scene
            SceneManager.LoadScene(Respawn);
        }
    }








}
