using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forces : MonoBehaviour
{
    //Creating rigidbody variable for object
    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        //Linking rigidbody variable to the object in unity
        rb = this.GetComponent<Rigidbody2D>();
    }

    void AddForceUpwards()
    {
        //Adding force to the y axis using impulse force
        rb.AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
    }


    // Update is called once per frame
    void Update()
    {
        //Check if the mouse button has been pressed
        if (Input.GetMouseButtonDown(0))
        {
            //Call AddForceUpwards method
            AddForceUpwards();
        }
    }
}
