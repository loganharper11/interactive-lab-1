using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : MonoBehaviour
{
    //Creating public variables for movement and jumping
    public float MovementSpeed = 1;
    public float JumpForce = 1;

    //Creating variable to track the knight's rigidbody
    private Rigidbody2D rigidbody;

    void Start()
    {
        //Loading rigidbody for knight on game startup
        rigidbody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        //Getting horizontal movement input from preset movement keys
        var movement = Input.GetAxis("Horizontal");

        //Moving the knight's position and changing speed based off user input
        transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * MovementSpeed;

        if (!Mathf.Approximately(0, movement))
            transform.rotation = movement < 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;

        //Getting jump movement input from preset key
        //Checking if player has jumped already to make sure they cannot double jump (By checking for y-axis velocity)
        if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidbody.velocity.y) < 0.001f)
        {
            //Adding force to the rigidbody using impulse force
            rigidbody.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
        }
    }
}