using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiate : MonoBehaviour
{
    //Creating new prefab
    public GameObject myPrefab;
    public float xPos = 0;
    public float yPos = 0;

    //Start is called before the first frame update
    void Start()
    {
        //Instantiating prefab at given quardinates
        Instantiate(myPrefab, new Vector3(xPos, yPos, 0), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
