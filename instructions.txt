Welcome to the Dungeon!

Use the "A" and "D" keys on the keyboard to move left and right.

Use the spacebar to jump.

The goal is to make it to the finish line and pickup the flag. 
Avoid the spikes as they will reset the character.

The new cave goblin just got some new Jordans', boy can he jump.
Test his jumps by pressing the mouse button.

Good Luck!